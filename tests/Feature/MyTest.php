<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class MyTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
          $res =  $this->json('post', '/testfile', [
            'file' => $file = UploadedFile::fake()->image('random.jpg')
        ]);

          $res -> assertStatus(200);
    }
}
